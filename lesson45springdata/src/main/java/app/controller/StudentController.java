package app.controller;

import app.model.Student;
import app.repositiry.StudentRepo;
import app.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;
    // temporary. ANTI-PATTERN
    private final StudentRepo studentRepo;

    // http://localhost:8080/student/1
    @GetMapping({"{id}"})
    public ResponseEntity<Student> get1(@PathVariable("id") Long id){
        Optional<Student> maybeStudent = studentService.findById(id);
        return ResponseEntity.of(maybeStudent);
    }

    @PostMapping
    public void create(@RequestBody Student student) {
        student.setId(null);
        studentRepo.save(student);
    }

    @PutMapping
    public void update(@RequestBody Student student) {
        studentRepo.save(student);
    }


    // http://localhost:8080/student/experiment
    @GetMapping("/experiment")
    public List<Student> get1() {
        return studentRepo.getStudentsByIdBetweenAndNameContainingIgnoreCase(1L, 100L, "i");
    }
}
