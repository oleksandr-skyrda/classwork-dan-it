package app.service;

import app.model.Student;
import app.repositiry.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepo repo;

    public Optional<Student> findById(Long id) {
        return repo.findById(id);
    }
}
