package app.repositiry;

import app.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
    //    @Query("select s from Student s where s.id > ? and s.id < ?")
    //    List<Student> getStudentsByIdBetween(Long id1, Long id2);

    // https://spring.io
    // https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
    // https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
    List<Student> getStudentsByIdBetweenAndNameContainingIgnoreCase(Long id1, Long id2, String substring);

}
