package app;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserProperties {
    public final static String keyName = "user_properties";

    String language;
    Integer resolution;

    public static UserProperties mkDefault(){
        return new UserProperties("UA", 3840);
    }
}
