package app;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("order")
@SessionAttributes(UserProperties.keyName)
public class OrderController {
    @ModelAttribute(UserProperties.keyName)
    public UserProperties initialValue() {
        return UserProperties.mkDefault();
    }

    // http://localhost:8080/order/s4?language=EN&resolution=1024
    // http://localhost:8080/order/s4?language=RU&resolution=1920
    // session set attribute
    @GetMapping("s4")
    public String handle_s4(@ModelAttribute(UserProperties.keyName) UserProperties properties,
                            @RequestParam("language") String lang,
                            @RequestParam("resolution") Integer res,
                            HttpSession session) {
        properties.setLanguage(lang);
        properties.setResolution(res);
        return String.format("Attributes set to %s %s", lang, res);
    }

    // session get attribute
    // http://localhost:8080/order/s5
    @GetMapping("s5")
    public String handle_s5(@ModelAttribute(UserProperties.keyName) UserProperties properties, HttpSession session) {
        return String.format("value from session: %s", properties);
    }
}
