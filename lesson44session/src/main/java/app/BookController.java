package app;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("book")
public class BookController {
    private final static String cookieName = "JID";
    private final static String s_key_resolution = "resolution";
    private final static String s_key_language = "language";

    private Optional<Cookie> find(HttpServletRequest rs) {
        return Optional.ofNullable(rs.getCookies())
                .flatMap(cc ->
                        Arrays.stream(cc)
                                .filter(c -> c.getName().equals(cookieName))
                                .findFirst()
                );
    }

    // http://localhost:8080/book/a
    // cookie set
    @GetMapping("a")
    public String handle_a(HttpServletResponse rs) {
        rs.addCookie(
                new Cookie(cookieName, UUID.randomUUID().toString())
        );
        return "cookie has been set";
    }

    // http://localhost:8080/book/b
    // remove
    @GetMapping("b")
    public String handle_b(HttpServletResponse rs) {
        Cookie cookie = new Cookie(cookieName, "");
        cookie.setMaxAge(0);
        rs.addCookie(cookie);
        return "cookie has been removed";
    }

    // http://localhost:8080/book/c
    // cookie access 1
    @GetMapping("c")
    public String handle_c(HttpServletRequest rq) {
        Optional<Cookie> maybeCookie = find(rq);
        String value = maybeCookie.map(Cookie::getValue).orElse("NOT_FOUND");
        return String.format("cookie %s accessed via HttpServletRequest: %s", cookieName, value);
    }

    // http://localhost:8080/book/d
    // cookie access 2
    @GetMapping("d")
    public String handle_d(@CookieValue(cookieName) Optional<Cookie> maybeCookie) {
        String value = maybeCookie.map(Cookie::getValue).orElse("NOT_FOUND");
        return String.format("cookie %s accessed via HttpServletRequest: %s", cookieName, value);
    }

    // http://localhost:8080/book/s1
    // session creation 1
    @GetMapping("s1")
    public String handle_s1(HttpServletRequest rq) {
        HttpSession session = rq.getSession();
        return String.format("session id via HttpServletRequest: %s", session.getId());
    }

    // http://localhost:8080/book/s2
    // session creation 2
    @GetMapping("s2")
    public String handle_s1(HttpSession session) {
        return String.format("session id via HttpSession: %s", session.getId());
    }

    // http://localhost:8080/book/s3
    // session invalidation
    @GetMapping("s3")
    public String handle_s3(HttpSession session) {
        String id = session.getId();
        session.invalidate();
        return String.format("session %s invalidated", id);
    }

    // http://localhost:8080/book/s4?language=EN&resolution=1024
    // http://localhost:8080/book/s4?language=RU&resolution=1920
    // session set attribute
    @GetMapping("s4")
    public String handle_s4(HttpSession session,
                            @RequestParam(s_key_language) String lang,
                            @RequestParam(s_key_resolution) Integer res) {
        session.setAttribute(s_key_language, lang);
        session.setAttribute(s_key_resolution, res);
        return String.format("Attributes set to %s %s", lang, res);
    }

    // session get attribute
    // http://localhost:8080/book/s5
    @GetMapping("s5")
    public String handle_s5(HttpSession session) {
        String lang = (String) session.getAttribute(s_key_language);
        Integer res = (Integer) session.getAttribute(s_key_resolution);
        return new StringBuilder("value from session:")
                .append(s_key_language)
                .append("=")
                .append(lang)
                .append(", ")
                .append(s_key_resolution)
                .append("=")
                .append(res)
                .toString();
    }


}
