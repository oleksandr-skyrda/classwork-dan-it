package app;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;

public class DBManagement {
    public static void migrate(String uri, String user, String password){
        FluentConfiguration cfg = new FluentConfiguration()
                .dataSource(uri, user, password);
        Flyway flyway = new Flyway(cfg);
        flyway.migrate();
    }
}
