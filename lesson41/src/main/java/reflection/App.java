package reflection;

import app.Ready;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class App {
    public static void main0(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> clazz = Class.forName("reflection.Point");
        Constructor<?> constructor = clazz.getConstructor();
        Object o = constructor.newInstance();
        System.out.println(o);
    }

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> clazz = Class.forName("reflection.Point2D");
        Constructor<?>[] constructors = clazz.getConstructors();
        for (Constructor<?> c: constructors){
            Class<?>[] parameterTypes = c.getParameterTypes();
            c.getParameterCount();
        }
        Constructor<?> constructor = clazz.getConstructor(int.class, int.class);
        Object o = constructor.newInstance(10, 20);
        Method method = clazz.getMethod("printMe");
        System.out.println(o); // "Point at[10, 20]"
        method.invoke(o); // "Point at[10, 20]"
        Annotation[] annotations = constructor.getAnnotations();
        for (Annotation a: annotations) {
            System.out.println(a); // @app.Ready(value=51, owner="Ben")
        }
        Ready annotation = constructor.getAnnotation(Ready.class);
        int value = annotation.value();
        String owner = annotation.owner();
        System.out.printf("v: %d, o: %s", value, owner); //v: 51, o: Ben
    }
}
