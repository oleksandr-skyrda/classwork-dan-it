package reflection;

import app.Ready;

public class Point2D {
    int x;
    int y;

    @Ready(value = 51, owner = "Ben")
    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("Point at[%d, %d]", x, y);
    }

    public void printMe(){
        System.out.println(this);
    }
}
