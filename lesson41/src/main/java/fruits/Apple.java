package fruits;

import fruits.a.Ready;
import fruits.a.RunMe;

@Ready(80)
public class Apple implements Fruit {
    @RunMe(2)
    public void whatever1() {
        System.out.println("Apple is ready");
    }
}
