package fruits;

import fruits.a.Ready;
import fruits.a.RunMe;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class App {
    public static void main(String[] args) {
        new Reflections(
                new ConfigurationBuilder()
                        .setScanners(new SubTypesScanner(false), new ResourcesScanner())
                        .addUrls(ClasspathHelper.forJavaClassPath())
                        .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("fruits")))
        ).getSubTypesOf(Fruit.class)
                .stream()
                .filter(c -> c.isAnnotationPresent(Ready.class))
                .filter(c -> c.getAnnotation(Ready.class).value() > 50)
                .forEach(c -> {
                    try {
                        Method[] methods = c.getMethods();
                        for (Method m: methods) {
                            if (m.isAnnotationPresent(RunMe.class)) {
                                Constructor<? extends Fruit> constructor = c.getConstructor();
                                Fruit fruit = constructor.newInstance();
                                RunMe ann = m.getAnnotation(RunMe.class);
                                int cnt = ann.value();
                                for (int x = 0; x < cnt; x ++) {
                                    m.invoke(fruit);
                                }
                            }
                        }
                    } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                             IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }
}
