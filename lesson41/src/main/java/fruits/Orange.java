package fruits;

import fruits.a.Ready;
import fruits.a.RunMe;

@Ready(90)
public class Orange implements Fruit{
    @RunMe(3)
    public void whatever2(){
        System.out.println("Orange is ready");
    }
}
