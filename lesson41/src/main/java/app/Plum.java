package app;

@Ready(value = 50, owner = "Jim")
public class Plum {
    @Override
    public String toString() {
        return "I'm Plum";
    }
}
