package app;

public @interface Ugly {
    int value() default 50;
}
