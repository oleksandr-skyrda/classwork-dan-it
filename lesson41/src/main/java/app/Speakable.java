package app;

public interface Speakable {
    String sound();
}
