package app;

public class Dog implements Speakable{
    @Override
    public String sound() {
        return "bark";
    }
}
