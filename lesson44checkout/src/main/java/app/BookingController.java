package app;

import app.session.CustomerDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@SessionAttributes(CustomerDetails.keyName)
@RequestMapping("/")
public class BookingController {

    @ModelAttribute(CustomerDetails.keyName)
    private CustomerDetails create0() {
        return new CustomerDetails();
    }

    // http://localhost:8080/booking
    @GetMapping("booking")
    private String handle1get(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return "1booking";
    }
    @PostMapping("booking")
    private RedirectView handle1post(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return new RedirectView("customer");
    }

    @GetMapping("customer")
    private String handle2get(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return "2customer";
    }
    @PostMapping("customer")
    private RedirectView handle2post(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return new RedirectView("payment");
    }

    @GetMapping("payment")
    private String handle3get(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return "3payment";
    }
    @PostMapping("payment")
    private RedirectView handle3post(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return new RedirectView("review");
    }

    @GetMapping("review")
    private String handle4get(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return "4review";
    }
    @PostMapping("review")
    private RedirectView handle4post(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return new RedirectView("confirm");
    }

    @GetMapping("confirm")
    private String handle5get(@ModelAttribute(CustomerDetails.keyName) CustomerDetails cd) {
        return "5confirm";
    }


}
