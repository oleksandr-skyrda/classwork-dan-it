package app;

import app.model.Book;
import app.model.BookCreateRq;
import app.model.BookUpdateRq;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("book")
public class BookController {
//    @GetMapping("{id}")
//    public Book bookById(@PathVariable("id") Integer id){
//        return new Book(id, "Java 17");
//    }
//    @GetMapping("{id}")
//    public ResponseEntity<Book> bookById(@PathVariable("id") Integer id){
//        Book book = new Book(id, "Java 17");
//        Optional<Book> maybeBook = Optional.ofNullable(book);
//
//        var resp1 = maybeBook.map(ResponseEntity::ok) // 0.5s
//                .orElse(ResponseEntity.notFound().build()); // 10s
//
//        var resp2 = maybeBook.map(ResponseEntity::ok) // 0.5s
//                .orElseGet(() -> ResponseEntity.notFound().build()); // 1h
//
//        return resp2;
//    }

    @GetMapping("{id}")
    public ResponseEntity<Book> bookById(@PathVariable("id") Integer id) {
        Book book = new Book(id, "Java 17", "Jimm");

        ResponseEntity<Book> resp1 = ResponseEntity.status(429).body(book);
        ResponseEntity<Book> resp2 = ResponseEntity.status(429).build();

        return resp2;
    }

//    @GetMapping("{id}")
//    public ResponseEntity<Book> bookById(@PathVariable("id") Integer id) {
//        Optional<Book> maybeBook = Optional.empty();
//        return ResponseEntity.of(maybeBook);
//    }
                                                        // book without id !!!!!
    //@PostMapping
    public ResponseEntity<?> saveBook1(@RequestBody Book book){
        // save to db(book) // potentially can overwrite book with id
        int id = 33;
        return ResponseEntity.created(URI.create("/book/" + id)).build();
    }

    //@PostMapping
    public ResponseEntity<?> saveBook2(@RequestBody BookCreateRq book){
        // id = save to db (book)
        int id = 33;
        return ResponseEntity.created(URI.create("/book/" + id)).build();
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping({"{id}"})
    public ResponseEntity<?> update(@PathVariable("id") Integer id, @RequestBody BookUpdateRq bookUpdateRq){
//        Book book = combine(bookUpdateRq, id);
//        bookUpdateRq.setId(id);
        if (true) return ResponseEntity.accepted().build();
        else return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
