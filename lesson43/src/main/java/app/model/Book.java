package app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Book {
    private Integer id;
    private String title;
    private String author;

//    public static void main(String[] args) {
//        Book book1 = new Book(17, "Java", "Jim");
//
//        Book book2 = Book.builder()
//                .id(17)
//                .title("Java")
//                .author("Jim")
//                .build();
//
//        System.out.println(book1);
//        System.out.println(book2);
//    }
}
