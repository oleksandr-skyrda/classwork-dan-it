package app.model;

import lombok.Data;

@Data
public class BookUpdateRq {
    private String title;
}
