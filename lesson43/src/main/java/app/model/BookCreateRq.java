package app.model;

import lombok.Data;

@Data
public class BookCreateRq {
    private String author;
}
