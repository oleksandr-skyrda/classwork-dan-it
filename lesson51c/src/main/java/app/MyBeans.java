package app;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Configuration
@Log4j2
@RequiredArgsConstructor
public class MyBeans {
    private final RestTemplate restTemplate;

    //    @Bean
    public CommandLineRunner run1() {
        return args -> {
            log.info("==============");
            String addr = "http://localhost:8080/person?id=11";

            ResponseEntity<Person> response = restTemplate.getForEntity(addr, Person.class);

            HttpStatus code = response.getStatusCode();
            log.info(code);

            Person body = response.getBody();
            log.info(body);

            log.info("==============");
        };
    }

    RequestEntity<?> make(String uri) {
        return RequestEntity
                .method(HttpMethod.GET, URI.create(uri))
                .header("Authorization", "token-given-by-api-provider")
                .build();
    }

    @Bean
    public CommandLineRunner run3() {
        return args -> {
            log.info("==============");
            String addr = "http://localhost:8080/person?id=11";

//            RequestEntity<?> entity = RequestEntity
//                    .method(HttpMethod.GET, URI.create(addr))
//                    .header("Authorization", "w4356346456456456")
//                    .body() // if Post
//                    .build();
            RequestEntity<?> ent = make(addr);

            restTemplate.exchange(ent, Person.class);

            log.info("==============");
        }

                ;
    }

    //    @Bean
    public CommandLineRunner run2() {
        return args -> {
            log.info("==============");
            String addr = "http://localhost:8080/person?id=11";

            Person body = restTemplate.getForObject(addr, Person.class);
            log.info(body);

            log.info("==============");
        };
    }

}
