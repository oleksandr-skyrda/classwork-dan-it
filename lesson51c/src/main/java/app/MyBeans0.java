package app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class MyBeans0 {
    @Bean
    public RestTemplate build(){
        return new RestTemplate();
    }
}
