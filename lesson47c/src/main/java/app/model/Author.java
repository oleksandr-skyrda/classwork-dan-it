package app.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "a_id")
    Integer id;
    @Column(name = "a_name")
    String name;
    @ManyToMany
    private Set<Book> books;
}
