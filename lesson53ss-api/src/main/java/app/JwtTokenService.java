package app;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@PropertySource("classpath:jwt.properties")
public class JwtTokenService {

  @Value("${jwt.secret}")
  private String jwtSecret;

  @Value("${jwt.expire.normal}")
  private Long expiration_normal; // 1 day (milliseconds)

  @Value("${jwt.expire.remember}")
  Long expiration_remember = 86400 * 1000L * 10; // 10 day (milliseconds) // убрали private, когда тестили класс X

//  private String jwtSecret = "SGVsbG9Xb3JsZA==";
//  private Long expiration_normal = 86400 * 1000L; // 1 day (milliseconds)
//  private Long expiration_remember = 86400 * 1000L * 10; // 10 day (milliseconds)

  public String generateToken(Integer userId, boolean rememberMe) {
    Date now = new Date();
    Date expiry = new Date(now.getTime() + (rememberMe ? expiration_remember : expiration_normal));
    return Jwts.builder().setSubject(userId.toString()).setIssuedAt(now).setExpiration(expiry).signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
  }

  public Optional<Jws<Claims>> tokenToClaims(String token) {
    try {
      return Optional.of(Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token));
    } catch (SignatureException ex) {
//      log.error();
    } catch (MalformedJwtException ex) {
//      log.error();
    } catch (ExpiredJwtException ex) {
//      log.error();
    } catch (UnsupportedJwtException ex) {
//      log.error();
    } catch (Exception ex) {
//      log.error();
    }
    return Optional.empty();
  }

  public Optional<Integer> extractTokenFromClaims(Jws<Claims> claims) {
    try {
      return Optional.of(claims.getBody().getSubject()).map(Integer::parseInt);
    } catch (Exception ex) {
      return Optional.empty();
    }
  }

  // jwt.io
  public static void main(String[] args) {
    JwtTokenService ts = new JwtTokenService();
    String t = ts.generateToken(8, false);
    Optional<Integer> maybeUserId = ts.tokenToClaims(t)
            .flatMap(ts::extractTokenFromClaims);

    System.out.println(t);
    System.out.println(maybeUserId);
  }
}
