package app.security;

import app.JwtTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {
  private static final String BEARER = "Bearer ";

  private final JwtTokenService tokenService;
  private final DbUserRepo dbUserRepo;
  private final MyUserDetailsServiceJPA userDetailsServiceJPA;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    try {
      extractFromRequest(request)
              .flatMap(tokenService::tokenToClaims)
              .flatMap(tokenService::extractTokenFromClaims)
              .map(JwtUserDetails::new)
              .map(ud -> new UsernamePasswordAuthenticationToken(ud, null, ud.getAuthorities()))
              .ifPresent(auth -> {
                auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(auth);
              });
      filterChain.doFilter(request, response);
    }catch (Exception x){
      // log
    }
  }

  private Optional<String> extractFromRequest(HttpServletRequest request) {
    return Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
            .filter(h -> h.startsWith(BEARER))
            .map(h -> h.substring(BEARER.length()));
  }
}
