package app.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

@Configuration
@RequiredArgsConstructor
public class DbUsersInitial {

    private final DbUserRepo dbUserRepo;
    private final PasswordEncoder enc;

    public void create(){
        dbUserRepo.saveAll(Arrays.asList(
           new DbUser("jim", enc.encode("123"), "USER"),
           new DbUser("jack", enc.encode("234"), "ADMIN")
        ));
    }
}
