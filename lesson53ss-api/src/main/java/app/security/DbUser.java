package app.security;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Optional;

@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
public class DbUser {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String username;
  private String password;
  private String roles;

  private final static String DELIMITER = ":";

  public DbUser(String username, String password, String... roles) {
    this.username = username;
    this.password = password;
    serRoles(roles);
  }

  public String[] gerRoles() {
    return Optional.ofNullable(roles)
            .map(x -> x.split(DELIMITER))
            .orElseGet(() -> new String[]{});
  }

  public void serRoles(String[] roles) {
    this.roles = String.join(DELIMITER, roles);
  }
}
