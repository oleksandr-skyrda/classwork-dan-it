package app;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * Сделали тест, какое из значений он возьмёт - из файла jwt.properties или поля JwtTokenService
 */
@Configuration
@RequiredArgsConstructor
public class X {

  final JwtTokenService ts;

  @Bean
  public CommandLineRunner handle(){
    return args -> System.out.println(ts.expiration_remember);
  }
}
