package app.ex;

import app.controller.BookController;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Log4j2
@ControllerAdvice
public class WholeAppExceptionHandler {
    record ApiErrorDetails(int code, String cause) {
    }

//    @ExceptionHandler({ApiError.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ApiErrorDetails handleExceptionSimple(ApiError ex) {
        return new ApiErrorDetails(
                555,
                ex.getClass().getSimpleName()
        );
    }

//    @ExceptionHandler({Exception.class})
    public ModelAndView handleExceptionPretty(RuntimeException ex) {
        log.error(String.format("Exception caught globally, class: %s", ex.getClass().getSimpleName()));
        log.error(String.format("Exception caught globally %s", ex.getMessage()));

        ModelAndView mov = new ModelAndView();
        mov.setViewName("global_error_template");
        mov.setStatus(HttpStatus.NOT_IMPLEMENTED);
        mov.addObject("ex_name", ex.getClass().getSimpleName());
//        mov.addObject("ex_stack", ex.getStackTrace());
        mov.addObject("ex_stack", ex.getMessage());

        return mov;
    }

}
