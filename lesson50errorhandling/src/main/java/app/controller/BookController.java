package app.controller;

import app.ex.AuthError;
import app.service.MyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping("book")
@RequiredArgsConstructor
public class BookController {
    private final MyService myService;

    // http://localhost:8080/book/index1
    @GetMapping("index1")
    public String handle1() {
        int result = myService.twice(1);
        return String.valueOf(result);
    }

    // http://localhost:8080/book/index21
    @GetMapping("index21")
    public String handle2v1() {
        try {
            int result = myService.twice(-1);
            return String.valueOf(result);
        } catch (IllegalArgumentException ex) {
            // f: IllegalArgumentException -> void
            return "WHATEVER DEFAULT";
        }
    }

    // http://localhost:8080/book/index22
    @GetMapping("index22")
    public String handle2_BAD() {
        String outcome;
        try {
            int result = myService.twice(-1);
            outcome = String.valueOf(result);
        } catch (IllegalArgumentException ex) {
            outcome = "WHATEVER DEFAULT";
        }
        return outcome;
    }

    // http://localhost:8080/book/index2
    @GetMapping("index2")
    public String handle2() {
        int result = myService.twice(-1);
        return String.valueOf(result);
    }

    record ErrorDetails(int code, String cause){}
//    @ExceptionHandler({RuntimeException.class, AuthError.class})
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    public ErrorDetails handleException(RuntimeException ex){
        log.error(ex.getMessage());
        // log to File
        // log to API
        return new ErrorDetails(111, ex.getMessage());
    }



}
