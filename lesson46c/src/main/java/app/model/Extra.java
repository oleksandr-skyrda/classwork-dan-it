package app.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Extra {
    @Id
    private Long Id;
    private String extraDetails;
    @OneToOne
    @MapsId
    private Student student;
}
