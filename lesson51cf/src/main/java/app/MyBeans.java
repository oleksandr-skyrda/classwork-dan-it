package app;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Log4j2
@RequiredArgsConstructor
public class MyBeans {
    private final PersonClient personClient;
    @Bean
    public CommandLineRunner run2() {
        return args -> {
            log.info("==============");
            Person person = personClient.getById(12);
            log.info(person);
            log.info("==============");
        };
    }

}
