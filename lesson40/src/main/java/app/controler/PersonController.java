package app.controler;

import app.model.LoginForm;
import app.model.Person;
import app.service.FormatterService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("person")
public class PersonController {
    private final FormatterService formatterService;

    public PersonController(@Qualifier("formatterServiceLower")
            FormatterService formatterService) {
        this.formatterService = formatterService;
    }

    @ResponseBody
    @PostMapping("form")
    public String doesntMatter9(@RequestBody LoginForm form){
        System.out.println(form);
        return "got";
    }

    @ResponseBody
    @PostMapping(value = "form2", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String doesntMatter8(@RequestBody LoginForm form){
        System.out.println(form);
        return "got";
    }

    @ResponseBody
    @PostMapping("form3")
    public String doesntMatter7(@RequestParam MultiValueMap body){
        System.out.println(body);
        return "got";
    }

    // http://localhost:8080/person/33
    @ResponseBody
    @GetMapping("{id}")
    public Person doesntMatter0(@PathVariable("id") Integer id0){
        return new Person(id0, "Jim");
    }
    // http://localhost:8080/person/test1
    @GetMapping("test1")
    public ResponseEntity<Person> nameDoesntMatter1(){
        Person p = new Person(33, "Jim");
        return ResponseEntity.status(200).body(p);
    }

    // http://localhost:8080/person/test2?id=77&name=Jim
    @ResponseBody
    @GetMapping("test2")
    public Person nameDoesntMatter2(@RequestParam("id") Integer id, @RequestParam("name") String name0){
        return new Person(id, name0);
    }
    // http://localhost:8080/person/test3
//    Controller -> RestController
//    @GetMapping("test3")
//    public Person nameDoesntMatter3(){
//        return new Person(33, "Jim");
//    }

    // http://localhost:8080/person/test3
    @ResponseBody
    @PostMapping("test3")
    public String nameDoesntMatter3(){
        //...
        return "Hello";
    }

    // http://localhost:8080/person/test4
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, path = "test4")
    public String nameDoesntMatter4(){
        //...
        return "Hello";
    }

    // http://localhost:8080/person/test5
    @ResponseBody
    @RequestMapping("test5")
    public String nameDoesntMatter5(HttpServletRequest rq){
        String method = rq.getMethod();
//        ...
        return "Hello";
    }
}
