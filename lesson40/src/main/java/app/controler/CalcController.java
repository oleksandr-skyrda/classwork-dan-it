package app.controler;

import app.service.CalcService;
import app.service.FormatterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("calc")
@RequiredArgsConstructor
public class CalcController {
    private final CalcService calcService;
    private final FormatterService formatterService;

    // http://localhost:8080/calc/add?x=1&y=2
    @GetMapping("add")
    public String add(@RequestParam("x") Integer x, @RequestParam("y") Integer y){
        int z = calcService.add(x, y);
        return String.format("%d + %d = %d", x, y, z);
    }
}
