package app.service;

import org.springframework.stereotype.Service;

@Service
public class FormatterServiceLower implements FormatterService{
    @Override
    public String format(String origin) {
        return origin.toLowerCase();
    }
}
