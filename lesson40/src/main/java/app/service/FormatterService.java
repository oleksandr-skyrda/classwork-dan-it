package app.service;

public interface FormatterService {
    String format(String origin);
}
