package app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CalcService {
    private final MathService mathService;

    public int add(int a, int b) {
        return mathService.add(a, b);
    }
}
