package app.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class FormatterServiceUpper implements FormatterService{
    @Override
    public String format(String origin) {
        return origin.toUpperCase();
    }
}
