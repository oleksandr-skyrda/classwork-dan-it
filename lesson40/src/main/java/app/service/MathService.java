package app.service;

import org.springframework.stereotype.Service;

@Service
public class MathService {
    public int add(int x, int y){
        return x + y;
    }
    public int sub(int x, int y){
        return x - y;
    }
}

