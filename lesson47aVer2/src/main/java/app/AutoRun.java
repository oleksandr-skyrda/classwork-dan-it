package app;

import app.model.Person;
import app.model.Phone;
import app.repo.PersonRepo;
import app.repo.PhoneRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Stream;

@Configuration
@Log4j2
@RequiredArgsConstructor
public class AutoRun {
    private final PersonRepo personRepo;
    private final PhoneRepo phoneRepo;

    //    @Bean
    public CommandLineRunner run1() {
        return args -> {
            log.info("::::::: autorun works");
        };
    }

//    @Bean
    public CommandLineRunner run2() {
        return args -> {
            log.info("entity creation w/o relations");
            List<Person> persons = Stream.of("Jeremy")
                    .map(Person::new)
                    .toList();
            personRepo.saveAll(persons);
        };
    }

//    @Bean
    public CommandLineRunner run3() {
        return args -> {
            log.info("entity creation WITH relations");

            // phone
            List<Phone> phones = Stream.of(
                    "111-11-111",
                    "111-11-112",
                    "111-11-113",
                    "111-11-114"
            ).map(Phone::new).toList();

            // person
            Person person = new Person("Sergio", phones);

            personRepo.save(person);
        };
    }

    @Bean
    public CommandLineRunner run4() {
        return args -> {
            log.info("fetching data");

            List<Person> all = personRepo.findAll();
            all.forEach(System.out::println);
        };
    }
}
