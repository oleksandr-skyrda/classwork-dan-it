package app.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Extra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "x_id")
    private Long Id;
    private String extraDetails;
    @OneToOne(mappedBy = "extra")
    private Student student;
}
