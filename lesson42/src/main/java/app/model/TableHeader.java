package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TableHeader {
    String id;
    String name;
    String price;
}
