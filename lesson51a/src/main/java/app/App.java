package app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class App {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();

        Person jim = new Person(33, "Jim");
        // serialization
        String json = om.writeValueAsString(jim);
        System.out.println(json); // {"id":33, "name":"Jim"}

        String raw = """
                {"id":33, "name":"Jim"}
                """;
        // deserialization
        Person person = om.readValue(raw, Person.class);
        System.out.println(person);
    }
}
