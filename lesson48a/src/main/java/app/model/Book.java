package app.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "b_id")
    Integer id;
    @Column(name = "b_title")
    String title;
    @ManyToMany(mappedBy = "books")
    private Set<Author> author;
}
