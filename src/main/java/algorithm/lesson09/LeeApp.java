package algorithm.lesson09;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LeeApp {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Lee lee = new Lee(15, 15);
//        Point from = Point.of(12,5);
        Point from = Point.of(13,7);
        Point to = Point.of(11, 3);
        List<Point> obstacles = List.of(
                Point.of(5, 11),

                Point.of(0, 0),
                Point.of(0, 1),
                Point.of(0, 2),
                Point.of(0, 3),
                Point.of(0, 4),
                Point.of(0, 5),
                Point.of(0, 6),
                Point.of(0, 7),
                Point.of(0, 8),
                Point.of(0, 9),
                Point.of(0, 10),
                Point.of(0, 11),
                Point.of(0, 12),
                Point.of(0, 13),
                Point.of(0, 14),

                Point.of(14, 0),
                Point.of(14, 1),
                Point.of(14, 2),
                Point.of(14, 3),
                Point.of(14, 4),
                Point.of(14, 5),
                Point.of(14, 6),
                Point.of(14, 7),
                Point.of(14, 8),
                Point.of(14, 9),
                Point.of(14, 10),
                Point.of(14, 11),
                Point.of(14, 12),
                Point.of(14, 13),
                Point.of(14, 14),

                Point.of(0,0),
                Point.of(1,0),
                Point.of(2,0),
                Point.of(3,0),
                Point.of(4,0),
                Point.of(5,0),
                Point.of(6,0),
                Point.of(7,0),
                Point.of(8,0),
                Point.of(9,0),
                Point.of(10, 0),
                Point.of(11, 0),
                Point.of(12, 0),
                Point.of(13, 0),
                Point.of(14, 0),

                Point.of(0,14),
                Point.of(1,14),
                Point.of(2,14),
                Point.of(3,14),
                Point.of(4,14),
                Point.of(5,14),
                Point.of(6,14),
                Point.of(7,14),
                Point.of(8,14),
                Point.of(9,14),
                Point.of(10,14),
                Point.of(11,14),
                Point.of(12,14),
                Point.of(13,14),
                Point.of(14,14)


        );
        Optional<List<Point>> trace = lee.trace(from, to, obstacles);
        System.out.println(trace);

        System.out.println((System.currentTimeMillis() - start) / 60);
        System.out.println((System.currentTimeMillis() - start));
    }
}
