package algorithm.lesson09;

public record LPoint(int x, int y) {
    @Override
    public String toString() {
        return String.format("[%2d, %2d]", x, y);
    }

    static LPoint of(int x, int y) {
        return new LPoint(x, y);
    }
    public LPoint move(int dx, int dy){
        return LPoint.of(x + dx, y + dy);
    }
}
