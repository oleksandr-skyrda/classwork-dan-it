INSERT INTO public.student (id, name, dob, g, done) VALUES (11, 'Jackson2', '2022-01-24', 1, true);
INSERT INTO public.student (id, name, dob, g, done) VALUES (10, 'Jackson', '2022-01-23', 2, true);
INSERT INTO public.student (id, name, dob, g, done) VALUES (5, 'Jeremy', null, 1, false);
INSERT INTO public.student (id, name, dob, g, done) VALUES (4, 'Sergio', null, 2, false);
INSERT INTO public.student (id, name, dob, g, done) VALUES (2, 'Ben', null, 2, false);
INSERT INTO public.student (id, name, dob, g, done) VALUES (3, 'Alex', null, 1, false);
INSERT INTO public.student (id, name, dob, g, done) VALUES (1, 'Jim', '2000-05-07', 1, false);
