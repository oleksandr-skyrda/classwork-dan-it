package web.lesson6;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

public class CalcServlet extends HttpServlet {
    private final CalcHistory log;

    public CalcServlet(CalcHistory log) {
        this.log = log;
    }

    // http://localhost:8080/calc?x=7&y=13
    private Optional<String> safeGet(HttpServletRequest req, String paramName) {
        return Optional.ofNullable(req.getParameter(paramName));
    }

    private Optional<Integer> safeToInt(String raw) {
        try {
            return Optional.of(Integer.parseInt(raw));
        } catch (NumberFormatException ex) {
            return Optional.empty();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String xs = req.getParameter("x");
        System.out.println(req.getQueryString()); //всё, что идёт посл знака ? в строке браузера
        Map<String, String[]> allParameter = req.getParameterMap();
        Optional<String> oy = safeGet(req, "y");
//        Optional <Optional<Integer>> oyi = oy.map(x -> Optional.ofNullable(Integer.parseInt(x))); //ниже flatMap упростит такую запись
//        Optional<Integer> oyi = oy.flatMap(x -> Optional.ofNullable(Integer.parseInt(x))); // ниже ещё добавим обработку
        Optional<Integer> oyi = oy.flatMap(x -> safeToInt(x));
//        if (oyi.isPresent()) oyi.get();
//        oyi.ifPresent(i -> System.out.println(i));

        int y = oyi.orElse(0); //not proper handing
        int x = Integer.parseInt(xs);
        int sum = x + y;

        try (PrintWriter writer = resp.getWriter()) {
            log.store(x, y, sum);
            writer.printf("%d + %d = %d", x, y, sum);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
