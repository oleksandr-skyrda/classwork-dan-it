package web.lesson8filter;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import web.lesson8filter.svc.CalcHistory;
import web.lesson8filter.svc.CalcSqlHistory;
import web.lesson8filter.svt.*;

import javax.servlet.DispatcherType;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.EnumSet;

public class ServerApp {
    private static final EnumSet<DispatcherType> ft = EnumSet.of(DispatcherType.REQUEST);
    // http://localhost:8080/calc?x=1&y=3
    // http://localhost:8080/calc?x=1&y=4
    // http://localhost:8080/calc?x=1&y=5
    // http://localhost:8080/calc?x=1&y=6
    // http://localhost:8080/history
    public static void main(String[] args) throws Exception {
        Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/fs1",
                "postgres",
                "pg123456");

        Server server = new Server(8080);

        ServletContextHandler handler = new ServletContextHandler();

        CalcHistory log =
//                new CalcInMemoryHistory();
                new CalcSqlHistory(conn);

        handler.addServlet(HelloServlet.class, "/hello");
        handler.addServlet(HandlePostServlet.class, "/post");

        CalcServlet calcServlet = new CalcServlet(log);
        HistoryServlet historyServlet = new HistoryServlet(log);

        handler.addServlet(new ServletHolder(calcServlet), "/calc");
        handler.addServlet(new ServletHolder(historyServlet), "/history");
        handler.addServlet(SetCookieServlet.class, "/login");
        handler.addServlet(RemoveCookieServlet.class, "/logout");

        handler.addFilter(CheckCookieFilter.class, "/calc", ft);
        handler.addFilter(CheckParamFilter.class, "/calc", ft);

        handler.addFilter(CheckCookieFilter.class, "/history", ft);

//        handler.addFilter(new FilterHolder(new CheckCookieFilter(...)), "/calc", ft);

        server.setHandler(handler);

        server.start();
        server.join();
    }
}
