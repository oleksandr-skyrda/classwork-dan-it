create table if not exists public.student
(
    id   integer not null
        constraint student_pk
            primary key,
    name text,
    dob  date,
    g    integer,
    done boolean
);

alter table public.student
    owner to postgres;

create table if not exists public.groupp
(
    id          integer not null
        constraint groupp_pk
            primary key,
    name        text,
    inserted_at timestamp default now()
);

alter table public.groupp
    owner to postgres;

create table if not exists public.person
(
    id     integer not null
        constraint person_pk
            primary key,
    name   text,
    salary double precision,
    age    integer
);

alter table public.person
    owner to postgres;

create index if not exists person_index
    on public.person (name);

create index if not exists person__index2
    on public.person (salary, age);

create table if not exists public.phones
(
    id  integer not null,
    num text
);

alter table public.phones
    owner to postgres;

create table if not exists public.author
(
    id   integer not null
        constraint author_pk
            primary key,
    name text
);

alter table public.author
    owner to postgres;

create table if not exists public.book
(
    id   integer not null
        constraint book_pk
            primary key,
    name text
);

alter table public.book
    owner to postgres;

create table if not exists public.ab
(
    a_id       integer not null
        constraint fk1
            references public.author
            on update cascade,
    b_id       integer not null
        constraint fk2
            references public.book,
    created_at timestamp default now(),
    constraint ab_pk
        primary key (a_id, b_id)
);

alter table public.ab
    owner to postgres;

create table if not exists public.location
(
    id   serial
        primary key,
    name text
);

alter table public.location
    owner to postgres;

create table if not exists public.move_assignment
(
    id  serial
        primary key,
    src integer
        constraint move_assignment_fk
            references public.location,
    dst integer
        constraint move_assignment_fk2
            references public.location
);

alter table public.move_assignment
    owner to postgres;

create table if not exists public.calc_history
(
    id serial
        primary key,
    a  integer,
    b  integer,
    c  integer,
    dt timestamp default now()
);

alter table public.calc_history
    owner to postgres;

create table if not exists public.messages
(
    id        serial
        primary key,
    user_from integer,
    user_to   integer,
    message   text,
    dt        timestamp default now()
);

alter table public.messages
    owner to postgres;

create table if not exists public.calc_history2
(
    id  serial
        primary key,
    a   integer,
    b   integer,
    c   integer,
    usr text,
    dt  timestamp default now()
);

alter table public.calc_history2
    owner to postgres;

create table if not exists public.calculator_user
(
    id     text not null
        primary key,
    login  text,
    passwd text
);

alter table public.calculator_user
    owner to postgres;

