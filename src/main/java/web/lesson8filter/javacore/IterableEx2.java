package web.lesson8filter.javacore;

public class IterableEx2 {
    public static void main(String[] args) {
        Month months = new Month();

        for (String month: months) {
            System.out.println(month);
        }
    }
}
