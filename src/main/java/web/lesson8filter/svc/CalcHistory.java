package web.lesson8filter.svc;

import java.sql.SQLException;
import java.util.List;

public interface CalcHistory {

    void store(String userId, int x, int y, int z) throws SQLException;
    List<String> getAll(String userId) throws SQLException;
}
