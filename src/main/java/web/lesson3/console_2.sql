create table if not exists account (
    id     serial constraint account_pk primary key,
    name   text,
    salary double precision
);

create table if not exists student(
    id   serial constraint student_pk primary key,
    name text,
    dob  date,
    g    integer
);

create table if not exists groupp (
    id          serial constraint groupp_pk primary key,
    name        text,
    inserted_at timestamp default now()
);

