package app.controller;

import app.model.TableHeader;
import app.model.TableItem;
import app.model.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;

@Controller
@RequestMapping("book")
public class BookController {
    @GetMapping("index")
    public String handle() {
        return "page";
    }

    @GetMapping("index123")
    public String oldLinkHandle() {
        return "redirect:index";
    }

    @GetMapping("index234")
    public RedirectView oldLinkHandle2() {
        return new RedirectView("index");
    }

    // http:localhost:8080/book/form
    @GetMapping("form")
    public String showForm() {
        return "form";
    }

    // http:localhost:8080/book/form
    @ResponseBody
    @PostMapping("form2")
    public String handleForm(LoginForm form) {
        // ....
        return form.toString();
    }

    // http:localhost:8080/book/tl
    @GetMapping("tl")
    public String thymeleaf(Model model){

        model.addAttribute("name", "Jim");

        model.addAttribute("header", new TableHeader("id", "name", "price"));
        ArrayList<TableItem> priceList = new ArrayList<>();
        priceList.add(new TableItem(5001, "Iphone 5", 500.5));
        priceList.add(new TableItem(5002, "Iphone 10", 1000d));
        priceList.add(new TableItem(5003, "Iphone 14", 1500d));
        model.addAttribute("lines", priceList);

        return "thymeleaf";
    }
}
