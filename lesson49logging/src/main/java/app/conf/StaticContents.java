package app.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@Configuration
@EnableWebMvc
public class StaticContents implements WebMvcConfigurer {
    private String[][] pageMappings = {
            // http   file (related tp templates folder)
            {"page2", "page2"},
            {"page3", "page3"}
    };

    private String[][] staticMappings = {
            // http     real file location
            {"/css/**", "classpath:/static/css/"},
            {"/img/**", "classpath:/static/img/"}
    };

//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("page2").setViewName("page2");
//        registry.addViewController("page3").setViewName("page3");
//    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        Arrays.stream(pageMappings)
                .forEach(mapping -> {
                    registry.addViewController(mapping[0]).setStatusCode(HttpStatus.OK).setViewName(mapping[1]);
                });

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        Arrays.stream(staticMappings)
                .forEach(mapping -> {
                    registry.addResourceHandler(mapping[0]).addResourceLocations(mapping[1]);
                });
    }
}
