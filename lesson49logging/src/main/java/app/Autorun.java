package app;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Configuration
public class Autorun {
    @Bean
    public CommandLineRunner runme(){
        return args -> {
            log.error("Hello from String"); // + + + + +
            log.warn("Hello from String"); // + + + +
            log.info("Hello from String"); // + + +
            log.debug("Hello from String"); // + +
            log.trace("Hello from String"); // +
        };
    }
}
