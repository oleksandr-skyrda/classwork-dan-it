package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TableItem {
    Integer id;
    String name;
    Double price;
}
