package app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {

    // http://localhost:8080/person
    @GetMapping("/person")
    @ResponseBody
    public Person handle(@RequestParam("id") Integer id){
        return new Person(id, "Jim");
    }
}
