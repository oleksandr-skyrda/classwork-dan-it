<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Подключаем css файл
    -->
    <link href="/static/css/1.css" rel="stylesheet">
    <title>Title</title>
</head>
<body>
    <img src="https://www.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png">
    <img src="/static/images/hp.png">

    <p>Hello, ${name}!</p>

    <table border="2">
        <thead>
            <td>Id</td>
            <td>Name</td>
            <td>Price, USD</td>
        </thead>
        <tbody>
        <#list price as line>
            <tr>
                <td>${line.id}</td>
                <td>${line.name}</td>
                <td>$${line.price}</td>
            </tr>
        </#list>
        </tbody>
    </table>
</body>
</html>