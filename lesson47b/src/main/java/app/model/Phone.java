package app.model;

import javax.persistence.*;

@Entity
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "p_id")
    Integer id;
    @Column(name = "p_number")
    String number;
    @ManyToOne(targetEntity = Person.class)
    @JoinColumn(name = "person_id")
    private Person person;
}
