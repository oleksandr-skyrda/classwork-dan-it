package app.security;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@Log4j2
public class MySecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
//                .antMatchers(HttpMethod.GET).permitAll() // работать будут только методы GET
//                .antMatchers(HttpMethod.GET, "/", "/guest").permitAll()
                .antMatchers("/", "/guest").permitAll()
                .antMatchers("/home").authenticated()
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/me").hasRole("USER")
                .antMatchers("/news").hasAnyRole("USER", "ADMIN")
                .anyRequest().authenticated();

        http.rememberMe();

        http.formLogin()
                .permitAll();
    }
//        public MySecurityConfig(DbUsersInitial initial){
//       log.info("======== creating");
//       initial.create();
//       log.info("======== created");
//    }
}
