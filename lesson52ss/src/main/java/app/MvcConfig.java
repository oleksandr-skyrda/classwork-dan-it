package app;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

  public void addViewControllers(ViewControllerRegistry registry) {
    String[][] mappings = {
    //  mapping     file name in `resources/templates` folder `.html`
        {"/"      , "menu" }, // open for ALL
        {"/guest" , "guest"}, // open for ALL
        {"/home"  , "home" }, // open for ANY authenticated
        {"/admin" , "admin"}, // only for ROLE: ADMIN
        {"/me"    , "me"   }, // open for ROLE: USER
        {"/news"  , "news" }, // open for ROLE: ADMIN | USER
    };

    for (String[] item: mappings) {
      registry.addViewController(item[0]).setViewName(item[1]);
    }
  }

}
