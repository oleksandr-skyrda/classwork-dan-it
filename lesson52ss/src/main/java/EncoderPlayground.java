import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

public class EncoderPlayground {

    public static void main(String[] args) {
        PasswordEncoder enc = new Pbkdf2PasswordEncoder();
        String rawPassword = "123";

//        String encoded1 = enc.encode(rawPassword);
//        System.out.println(encoded1); // f6ec3129de25c8aeecfffc63fb9198583ff1331b98793f76c5f6b7262f219fc6438640d23950055c
//
//        String encoded2 = enc.encode(rawPassword);
//        System.out.println(encoded2); // 843b515de4bac87fd0d57854590fc34893ee9e3ba8124214b960bba88ab7aa945e7230b58f41d4e1
//
//        boolean m1 = enc.matches("123", encoded1);
//        boolean m2 = enc.matches("123", encoded2);
//        boolean m3 = enc.matches("123", "843b515de4bac87fd0d57854590fc34893ee9e3ba8124214b960bbb88ab7aa945e7230b58f41d4e1");
//
//        System.out.println(m1); // true
//        System.out.println(m2); // true
//        System.out.println(m3); // false

        String encode1 = enc.encode(rawPassword);
        System.out.println(encode1);

        String encode2 = enc.encode(rawPassword);
        System.out.println(encode2);

        System.out.println(enc.matches(rawPassword, encode1));
        System.out.println(enc.matches(rawPassword, encode2));
    }

}
