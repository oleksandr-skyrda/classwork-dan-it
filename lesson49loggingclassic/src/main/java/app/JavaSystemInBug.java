package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class JavaSystemInBug {

    public static void main0(String[] args) {
        Scanner s1 = new Scanner(System.in);
        String l1 = s1.next();
        s1.close();

        Scanner s2 = new Scanner(System.in);
        String l2 = s2.next(); // java.util.NoSuchElementException
        s2.close();
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
        String l1 = br1.readLine();
        br1.close();

        BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
        String l2 = br2.readLine();
        br2.close();
    }

}
