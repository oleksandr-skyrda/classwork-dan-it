package app;

import java.util.Optional;
import java.util.function.Supplier;

public interface Logger {

    int getCurrentLevel();

    void log(String s);

    default void logWithLevel(int level, String message) {
        if (getCurrentLevel() < level) log(message);
    }

    default void fatal(String message) {
        logWithLevel(9, message);
    }
    default void error(String message) {
        logWithLevel(7, message);
    }
    default void warning(String message) {
        logWithLevel(5, message);
    }
    default void info(String message) {
        logWithLevel(4, message);
    }
    default void debug(String message) {
        logWithLevel(2, message);
    }
    default void trace(String message) {
        logWithLevel(0, message);
    }

    default void logWithLevel(int level, Supplier<String> message) {
        if (getCurrentLevel() < level) log(message.get());
    }

    default void trace(Supplier<String> message) {
        logWithLevel(0, message);
    }

}
