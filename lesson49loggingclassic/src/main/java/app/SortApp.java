package app;

import lombok.extern.slf4j.Slf4j;
//import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;

import java.util.Arrays;
import java.util.stream.Stream;

@Slf4j
public class SortApp {

//    static Logger log = LoggerFactory.getLogger(SortApp.class);
//    static Logger logger = ConsoleLogger.make();
//    static Logger logger = NoLogger.make();

    static int accessCount = 0;
    static int swapCount = 0;
    public static void sort(int[] xs, Console console) {
        console.print("proper way to print out");
        for (int i=0; i< xs.length; i++)
            for (int j=i+1; j < xs.length; j++) {
//                Optional<?> x = null;
//                Object x1 = x.orElse(destroytheUniverse());
//                Object x2 = x.orElseGet(() -> destroytheUniverse());
//                logger.trace(() -> String.format("i:%d xs[%d]:%d j:%d xs[%d]:%d", i, i, xs[i], j, j, xs[j]));
                log.trace(String.format("i:%d xs[%d]:%d j:%d xs[%d]:%d", i, i, xs[i], j, j, xs[j]));
                accessCount++;
                if (xs[i] > xs[j]) {
                    log.trace("-- swapping");
                    int x = xs[i];
                    xs[i] = xs[j];
                    xs[j] = x;
                    log.trace(Arrays.toString(xs));
                    swapCount++;
                } else {
                    log.trace("-- skipping");
                }

            }
    }

    public static void main(String[] args) {
        Console console = new Console();
        log.info("app started");
        int[] random = Stream
                .generate(() -> (int) (Math.random() * 100))
                .mapToInt(a -> a)
                .limit(10)
                .toArray();
        log.debug(Arrays.toString(random));
        sort(random, console);
        log.debug(Arrays.toString(random));
        log.debug(String.format("AccessCount: %d\n", accessCount));
        log.info(String.format("SwapCount: %d\n", swapCount));
    }

}
