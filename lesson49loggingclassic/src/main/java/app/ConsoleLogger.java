package app;

public class ConsoleLogger {

    public static Logger make() {

        return new Logger() {

            @Override
            public int getCurrentLevel() {
                // - constructor (not flexible)
                // - can be read from configuration file (not flexible)
                //
                // - app argument
                //                    environment
                //                    property
                String logLevelProp = System.getProperty("LOG_LEVEL"); // java -jar ..... -DLOG_LEVEL=3
                String logLevelEnv  = System.getenv("LOG_LEVEL");
                return 3;
            }

            @Override
            public void log(String s) {
                System.out.println(s);
            }
        };

    }
}
