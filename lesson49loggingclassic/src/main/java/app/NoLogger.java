package app;

public class NoLogger {

    public static Logger make() {

        return new Logger() {
            @Override
            public int getCurrentLevel() {
                return 0;
            }

            @Override
            public void log(String s) {}

        };

    }
}
